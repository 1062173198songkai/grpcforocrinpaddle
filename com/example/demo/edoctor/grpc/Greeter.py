from concurrent import futures
import sys

import grpc
import time
import os
import argparse

import ocr_pb2 as ocr_pb2
import ocr_pb2_grpc as ocr_pb2_grpc
sys.path.append(os.path.abspath(os.path.join(os.getcwd(), "../../../../../")))
print(os.path.abspath(os.path.join(os.getcwd(), "../../../../../")))
print(sys.path)

import tools.infer.predict_system as predict_system


class Greeter(ocr_pb2_grpc.GreeterServicer):

    def SendOcr(self, request, context):
        print("收到请求")
        img_bytes = request.pics
        pic_name = request.picname
        current_time = time.time()
        print(current_time)
        pic_path = str(current_time) + pic_name
        # current_path = os.path.abspath(__file__)
        # father_path = os.path.abspath(os.path.dirname(current_path) + os.path.sep + ".")
        # pic_absolute_path = os.path.join(os.path.abspath(os.path.dirname(current_path) + os.path.sep + ".."),
        # pic_absolute_path = "." + os.path.sep + "com" + os.path.sep + "example" + os.path.sep + "demo" + os.path.sep + "edoctor" + os.path.sep + "grpc" + os.path.sep + pic_path
        pic_absolute_path = "." + os.path.sep + pic_path
        print(pic_absolute_path)

        # pic_file = open(pic_path, "w+")
        # pic_absolute_path = os.path.abspath(__file__)
        with open(pic_path, "wb") as fd:
            fd.write(img_bytes)
            fd.close()
            print(pic_absolute_path)
        args = argparse.Namespace()
        args.image_dir = pic_absolute_path
        args.det_model_dir = '../../../../../inference/ch_ppocr_server_v2.0_det_infer/'
        args.rec_model_dir = '../../../../../inference/ch_ppocr_server_v2.0_rec_infer/'
        args.cls_model_dir = '../../../../../inference/ch_ppocr_mobile_v2.0_cls_infer/'
        args.cls_batch_num = 6
        args.cls_image_shape = '3, 48, 192'
        args.cls_thresh = 0.9
        args.det_algorithm = 'DB'
        args.det_db_box_thresh = 0.5
        args.det_db_thresh = 0.3
        args.det_db_unclip_ratio = 1.6
        args.det_east_cover_thresh = 0.1
        args.det_east_nms_thresh = 0.2
        args.det_east_score_thresh = 0.8
        args.det_limit_side_len = 960
        args.det_limit_type = 'max'
        args.det_sast_nms_thresh = 0.2
        args.det_sast_polygon = False
        args.det_sast_score_thresh = 0.5
        args.drop_score = 0.5
        args.enable_mkldnn = False
        args.gpu_mem = 500
        args.ir_optim = True
        args.label_list = ['0', '180']
        args.max_batch_size = 10
        args.max_text_length = 25
        args.rec_algorithm = 'CRNN'
        args.rec_batch_num = 6
        args.rec_char_dict_path = '../../../../../ppocr/utils/ppocr_keys_v1.txt'
        args.rec_char_type = 'ch'
        args.rec_image_shape = '3, 32, 320'
        args.use_dilation = False
        args.use_fp16 = False
        args.use_gpu = False
        args.use_pdserving = False
        args.use_tensorrt = False
        args.vis_font_path = '../../../../../doc/fonts/simfang.ttf'
        args.use_angle_cls = True
        args.use_space_char = True
        # args = {'image_dir': pic_absolute_path, 'det_model_dir': './inference/ch_ppocr_server_v2.0_det_infer/',
        #         'rec_model_dir': './inference/ch_ppocr_server_v2.0_rec_infer/',
        #         'cls_model_dir': './inference/ch_ppocr_mobile_v2.0_cls_infer/',
        #         'use_angle_cls': True, 'use_space_char': True}
        ocr_result = predict_system.detect(args)
        print(ocr_result)
        print(type(ocr_result))
        result_str = ""
        for text, score in ocr_result:
            result_str += text + "_"
        return ocr_pb2.OcrReply(result=result_str)


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    ocr_pb2_grpc.add_GreeterServicer_to_server(Greeter(), server)
    server.add_insecure_port('[::]:19021')
    server.start()
    server.wait_for_termination()


if __name__ == '__main__':
    print("启动")
    current_path = os.path.abspath(__file__)
    dirname = os.path.dirname(current_path)
    father_path = os.path.abspath(os.path.dirname(current_path) + os.path.sep + ".")

    print(current_path)
    print(dirname)
    print(father_path)
    print(os.path.abspath(os.path.join(os.getcwd(), "../../../../../")))
    # logging.basicConfig()
    serve()
    print("19021启动成功")
